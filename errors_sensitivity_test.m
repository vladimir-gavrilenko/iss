clear, clc
rp = 0.150; % radius of the platform
rb = 0.175; % radius of the base
h  = 0.300; % height in initial position
Cb = 0.01; % base attachment points distance
Cp = 0.01; % platform attachment points distance
[base, platform] = stewart_hexagons(rb, rp, Cb, Cp);

draw = false;
t_pause = 0.5;

linear_xy_diapasone = [0, -1, 1] .* (0.1);
linear_z_diapasone = [0, -1, 1] .*  (0.025);
orientation_diapasone = [0, -1, 1] .* (7 * pi / 180);
qe0 = [0; 0; h; 0; 0; 0];
qe = zeros(6, 3^6);
max_e_L = zeros(1, 3^6);
min_e_L = zeros(1, 3^6);

i = 1;
for dx = linear_xy_diapasone
  for dy = linear_xy_diapasone
    for dz = linear_z_diapasone
      for dphi = orientation_diapasone
        for dtheta = orientation_diapasone
          for dpsi = orientation_diapasone
            dqe = [dx; dy; dz; dphi; dtheta; dpsi];
            qe(:,i) = qe0 + dqe;
            i = i + 1;
          end
        end
      end
    end
  end
end

N = max(size(qe));
for i = 1 : 1 : N
    q = qe_to_q(qe(:,i));
    [L, bp, pp] = stewart_invkin(base, platform, q);
    [max_e_L(i), min_e_L(i)] = stewart_errors_sensitivity(base, platform, qe(:,i));

    if draw
        stewart_draw(bp, pp);
        pause(t_pause);
    else
        disp(i)
    end
end



