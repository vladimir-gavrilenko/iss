clear, clc

Rb = 0.175;
Rp = 0.050 : 0.01 : 0.150;
H0 = 0.300 : 0.02 : 0.500;
dx = 0.100; dy = 0.100; dz = 0.025; da = 7 * pi / 180;

b = 1; p = 2;
C = [   0.00, 0.00;
        0.03, 0.03;
        0.05, 0.05;
        0.08, 0.08;    ];

[URp, UH0] = meshgrid(Rp, H0);
[N, M] = size(URp);
S = zeros(max(size(C)), N, M);
for ic = 1 : 1 : 4
    tic
    for i = 1 : 1 : max(size(URp))
        for j = 1 : 1 : max(size(UH0))
            S(ic, i, j) = stewart_errors_sensitivity_from_params(Rb, ...
                URp(i,j), C(ic,b), C(ic,p), UH0(i,j), dx, dy, dz, da);
            disp([ic, i, j]);
        end
    end
    toc
    subplot(2,2,ic);
    axis( [ 0.2, 0.9, 1.7, 2.9, 0.01, 0.05 ] );
    h = gca;
    grid on
    set(h, 'View', [ 130, 26 ]);
    hold on
    S0 = reshape(S(ic,:,:), size(URp));
    surf(URp./Rb, UH0./Rb, S0);
    xlabel('Rp/Rb'); ylabel('H0/Rb'); zlabel('max{S_{ij}}');
    s1 = strcat('Cb= ', num2str(C(ic,b)));
    s2 = strcat(', Cp= ', num2str(C(ic,p)));
    title(strcat(s1, s2));
end
