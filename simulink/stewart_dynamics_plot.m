if step_ref > 0
    err_q = qe - qe_ref;
    err_pos = err_q.Data(:,1:3) .* 1e6;
    err_ang = err_q.Data(:,4:6) .* (180 / pi) .* 3600;
    t = err_q.Time;
    
    figure
    
    subplot(2,1,1);
    plot(t, err_pos);
    axis([0 t(end) (min(err_pos(:)) - 0.2*abs(min(err_pos(:)))) (max(err_pos(:)) + 0.2*abs(max(err_pos(:))))]);
    grid on
    xlabel('t [c]');
    ylabel('e_x, e_y, e_z [���]');
    legend('e_x','e_y','e_z');
    title('����������� ������');

    subplot(2,1,2);
    plot(t, err_ang);
    axis([0 t(end) (min(err_ang(:)) - 0.2*abs(min(err_ang(:)))) (max(err_ang(:)) + 0.2*abs(max(err_ang(:))))]);
    grid on
    xlabel('t [c]');
    ylabel('e_{\phi}, e_{\theta}, e_{\psi} [���.���]');
    legend('e_{\phi}', 'e_{\theta}', 'e_{\psi}');
    title('������� ������');
    
    %---
    figure
    
    subplot(2,1,1);
    i0 = find(t >= 0.9);
    plot(t(i0), err_pos(i0,:));
    axis([0.9 t(end) -10 10]);
    grid on
    xlabel('t [c]');
    ylabel('e_x, e_y, e_z [���]');
    legend('e_x','e_y','e_z');
    title('����������� ������');

    subplot(2,1,2);
    plot(t(i0), err_ang(i0,:));
    axis([0.9 t(end) -10 10]);
    grid on
    xlabel('t [c]');
    ylabel('e_{\phi}, e_{\theta}, e_{\psi} [���.���]');
    legend('e_{\phi}', 'e_{\theta}', 'e_{\psi}');
    title('������� ������');
    
    %--
    figure
    subplot(2,1,1);
    plot(t, eL.Data .* 1e6);
    axis([0 t(end) -2000 2000]);
    grid on
    xlabel('t [c]'); ylabel('e_L [���]'); legend('e_{L1}','e_{L2}','e_{L3}',...
           'e_{L4}','e_{L5}','e_{L1}');
    title('������ �������� ��������');
    
    subplot(2,1,2);
    plot(t(i0), eL.Data(i0,:) .* 1e6);
    grid on
    xlabel('t [c]'); ylabel('e_L [���]'); legend('e_{L1}','e_{L2}','e_{L3}',...
           'e_{L4}','e_{L5}','e_{L1}');
end
