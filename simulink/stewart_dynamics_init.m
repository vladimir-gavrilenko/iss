clear, clc

mfilepath = mfilename('fullpath');
addpath(fullfile(mfilepath,'../img'));
addpath(fullfile(mfilepath,'../../stewart'));
addpath(fullfile(mfilepath,'../../util'));
step_ref = 0; % ������������ ������

if step_ref > 0
    T_sim = 1;
    max_step_size = 1e-5;
    relative_tolrance = 1e-5;
    output_times = 0:max_step_size:T_sim;
else
    T_sim = 18*2;
    max_step_size = 5e-4;
    relative_tolrance = 1e-3;
    output_times = 0:1/30:T_sim;
end

Rp = 0.150; % ������ ���������, �
Rb = 0.175; % ������ ���������, �
H  = 0.300; % ��������� ������, �
Cb = 0.050; % ���������� ����� ��������� ���������, �
Cp = 0.050; % ���������� ����� ��������� ���������, �
rho_b = 2.7; % ��������� ���������, �/��^3
rho_p = 2.7; % ��������� ���������, �/��^3
h_b = 0.01; % ������� ���������, �
h_p = 0.005; % ������� ���������, �
% ���������� ��������������� ��������� � ���������
[base, platform] = stewart_hexagons(Rb, Rp, Cb, Cp);
color_b = [0.2 0.8 0.2];
color_p = [0.8 0.2 0.2];

q0 = qe_to_q([0; 0; H; 0; 0; 0;]);
[L0, B0, P0] = stewart_invkin(base, platform, q0);

lower_leg_len = L0(1) / 2;
lower_leg_R = 0.008;
lower_leg_r = 0.004;
lower_leg_rho = 2.7;
lower_leg_color = [0.1 0.2 0.8].*0.7;

upper_leg_len = L0(1) * 3 / 4;
upper_leg_R = lower_leg_r;
upper_leg_r = upper_leg_R / 4;
upper_leg_rho = 2.7;
upper_leg_color = [0.1 0.2 0.8].*1.2;

Kp = 500;
Ki = 4;
Kd = 100;