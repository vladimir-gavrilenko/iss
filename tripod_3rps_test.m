clear, clc
rp = 0.175; % radius of the platform
rb = 0.175; % radius of the base
[base, platform] = tripod_3rps_triangles(rb, rp);

L0 = [0.3; 0.3; 0.3];
Ls = [-1, -0.5, 0, 0.5, 1] .* L0(1) .* 0.3;
N = (length(Ls))^3;
q = zeros(6,N);
i = 1;
L = zeros(3,N);
for dL1 = Ls
    for dL2 = Ls
        for dL3 = Ls
            L(:,i) = L0 + [dL1; dL2; dL3];
            q(:,i) = tripod_3rps_forkin(base, platform, L(:,i));
            i = i + 1;
        end
    end
end

Lout = zeros(3,N);
for i = 1 : 1 : N
    [Lout(:,i), bp, pp] = tripod_3rps_invkin(base, platform, q(:,i));
    tripod_3rps_draw(bp, pp);
    pause(0.1);
end

plot3(q(4,:), q(5,:), q(6,:));
