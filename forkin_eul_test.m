rp = 0.150; % radius of the platform
rb = 0.175; % radius of the base
h  = 0.300; % height in initial position
Cb = 0.100; % base attachment points distance
Cp = 0.100; % platform attachment points distance
[base, platform] = stewart_hexagons_eul(rb, rp, Cb, Cp);

qe = zeros(6,10);
for i = 1 : 1 : 10
    qe(:,i) = q_to_qe(q(:,i));
end
n = zeros(10,1);
L = zeros(6,10);
qe_out = zeros(6,10);
for i = 1:1:10
    L(:,i) = stewart_invkin_eul(base, platform, qe(:,i));
    [qe_out(:,i), n(i)] = stewart_forkin_eul(base, platform, L(:,i));
    [tmp, b, p] = stewart_invkin_eul(base, platform, qe_out(:,i));
    h = stewart_draw(b, p);
%     file_name = strcat('forkin_fig_', num2str(i));
%     saveas(h, file_name, 'png');
    pause(0.5)
end