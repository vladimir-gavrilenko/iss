function [L, base_pos, platform_pos] = tripod_xLxS_invkin( base, platform, q )
coder.inline('never');
L = zeros(3,1);
base_pos = zeros(3,3);
platform_pos = zeros(3,3);

% convert 4x6 legs positions matricies to 3x6 (base_pos, platform_pos)
delete_last_row = [ 1 0 0 0
                    0 1 0 0
                    0 0 1 0 ];

xx = q(1);  yy = q(2);  zz = q(3);% abs or rel CS?
R = quat_to_rotm(q(4:7)');

% Transformation matrices
Tp = [R, [xx; yy; zz]; 0 0 0 1] ;
Tb = [eye(3), [0;0;0]; 0 0 0 1];
for i = 1 : 1 : 3
    platform_pos(:,i) = delete_last_row*(Tp * [ platform(i,:)' ; 0; 1; ]);
    base_pos(:,i)     = delete_last_row*(Tb * [ base(i,:)'     ; 0; 1; ]);

    L(i) = sqrt((platform_pos(1,i) - base_pos(1,i))^2 + ...
                (platform_pos(2,i) - base_pos(2,i))^2 + ...
                (platform_pos(3,i) - base_pos(3,i))^2 );
end

end
