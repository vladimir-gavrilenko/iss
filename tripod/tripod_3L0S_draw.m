function [ h ] = tripod_3L0S_draw( base_pos, platform_pos )
clf
hold on
axis( [ -0.35, 0.35, -0.35, 0.35, 0, 0.55 ] );
h = gca;
grid on
set(h, 'View', [ 36, 22 ]);

for i = 1 : 1 : 3
    line( [ base_pos(1,i) platform_pos(1,i) ], ...
          [ base_pos(2,i) platform_pos(2,i) ], ...
          [ base_pos(3,i) platform_pos(3,i) ], ...
          'LineWidth', 2, 'Color', [.10 .5 .8]);
end
fill3( base_pos(1,:), base_pos(2,:), base_pos(3,:), [0 1 0] )
fill3( platform_pos(1,:), platform_pos(2,:), platform_pos(3,:), [1 0 0])
xlabel('x')
ylabel('y')
zlabel('z')
hold off

end
