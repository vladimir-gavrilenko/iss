function [ q, n ] = tripod_xLxS_forkin( base, platform, L0 )

base6 = zeros(6,2);
platform6 = zeros(6,2);
L6 = [L0(1); L0(1); L0(2); L0(2); L0(3); L0(3);];
for i = 1 : 1 : 3
    base6(2*i-1,:) = base(i,:);
    base6(2*i,:) = base(i,:);
    platform6(2*i-1,:) = platform(i,:);
    platform6(2*i,:) = platform(i,:);
end

[q, n] = stewart_forkin(base6, platform6, L6);

end
