function [ base, platform ] = tripod_xLxS_triangles( rb, rp )
base = zeros(3,2);
platform = zeros(3,2);
phi = 0;
for i = 1 : 1 : 3
    base(i,:)   = [ rb*cos(phi),  rb*sin(phi)];
    platform(i,:) = [ rp*cos(phi), rp*sin(phi)];
    phi = phi + 2*pi/3;
end
end
