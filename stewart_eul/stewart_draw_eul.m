function stewart_draw_eul( base_pos, platform_pos )
%close all
clf
hold on
axis( [ -0.2, 0.2, -0.2, 0.2, 0, 0.6 ] );
h1 = gca;
grid on
set(h1, 'View', [ 36, 22 ]);

for i = 1 : 1 : 6
    line( [ base_pos(1,i) platform_pos(1,i) ], ...
          [ base_pos(2,i) platform_pos(2,i) ], ...
          [ base_pos(3,i) platform_pos(3,i) ], ...
          'LineWidth', 2, 'Color', [.10 .5 .8]); 
end

fill3( base_pos(1,:), base_pos(2,:), base_pos(3,:), [0 1 0] )
fill3( platform_pos(1,:), platform_pos(2,:), platform_pos(3,:), [1 0 0])
xlabel('x')
ylabel('y')
zlabel('z')
hold off

end

