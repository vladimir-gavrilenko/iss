function [ base, platform ] = stewart_hexagons_eul( rb, rp, Cb, Cp )
base = zeros(6,2);
platform = zeros(6,2);

phi = 0;
dphi_b = asin( Cb / rb / 2 );
dphi_p = asin( Cp / rp / 2 );
for i = 1 : 2 : 6
    base(i,:)   = [ rb*cos(phi-dphi_b),  rb*sin(phi-dphi_b)];
    base(i+1,:) = [ rb*cos(phi+dphi_b),  rb*sin(phi+dphi_b)];

    alpha = phi + pi/2;
    platform(i,:)   = [ rp*cos(alpha-pi/3+dphi_p), rp*sin(alpha-pi/3+dphi_p)];
    platform(i+1,:) = [ rp*cos(alpha+pi/3-dphi_p), rp*sin(alpha+pi/3-dphi_p)];

    phi = phi + 2*pi/3;
end

end
