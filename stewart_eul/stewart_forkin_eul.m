function [ qe, n ] = stewart_forkin_eul( base, platform, L0 )
qe = [0; 0; 0.5; 0; 0; 0;]; % initial etimation
F = stewart_invkin_eul(base, platform, qe) - L0;
eps = 1e-6;
n = 0;
while ( norm(F) > eps )
    inv_J = inv_J_eul(base, platform, qe);
    qe = qe - pinv(inv_J)* F;
%     qe = qe - inv_J \ F; % - doesn`t work
    L = stewart_invkin_eul(base, platform, qe);
    F = L - L0;
    n = n + 1;
end

end

function [ inv_J ] = inv_J_eul( base, platform, qe )

phi = qe(4); theta = qe(5); psi = qe(6);
sph = sin(phi);   cph = cos(phi);
sth = sin(theta); cth = cos(theta);
sps = sin(psi);   cps = cos(psi);

w__Rp0 = [ cps*cph - cth*sph*sps,  -sps*cph - cth*sph*cps,   sth*sph ;
           cps*sph + cth*cph*sps,  -sps*sph + cth*cph*cps   -sth*cph ;
           sps*sth,                 cps*sth                  cth     ;  ];
% w__Rp0 = eul2rotm(qe(4:6)', 'ZYZ');
p0__Rp = [  0   1   0
           -1   0   0
            0   0   1];

w__Rp = w__Rp0 * p0__Rp;

p__a = [platform'; zeros(1,6)];

[L, w__b, w__a] = stewart_invkin_eul(base, platform, qe);
n = zeros(3,6);
inv_J1 = zeros(6,6);
for i = 1 : 1 : 6
    n(:,i) = (w__a(:,i) - w__b(:,i))./L(i);
    inv_J1(i,:) = [n(:,i)', ( cross( w__Rp * p__a(:,i) , n(:,i) ) )'];
end

ij2 = [   0   cph   sph*sth
          0   sph  -cph*sth
          1   0     cth      ];
inv_J2 = [ eye(3), zeros(3,3); zeros(3,3), ij2 ];

inv_J = inv_J1 * inv_J2;
end
