function [L, base_pos, platform_pos] = stewart_invkin_eul( base, platform, qe )
x = qe(1:3);
phi = qe(4); theta = qe(5); psi = qe(6);
sph = sin(phi);   cph = cos(phi);
sth = sin(theta); cth = cos(theta);
sps = sin(psi);   cps = cos(psi);

w__Rp0 = [ cps*cph - cth*sph*sps,  -sps*cph - cth*sph*cps,   sth*sph ;
           cps*sph + cth*cph*sps,  -sps*sph + cth*cph*cps   -sth*cph ;
           sps*sth,                 cps*sth                  cth     ;  ];
% w__Rp0 = eul2rotm(qe(4:6)', 'ZYZ');
p0__Rp = [  0   1   0
           -1   0   0
            0   0   1];

w__Rp = w__Rp0 * p0__Rp;

p__a = [platform'; zeros(1,6)];

b = [base'; zeros(1,6)];
a = zeros(3,6);
L = zeros(6,1);
for i = 1 : 1 : 6
    a(:,i) = x + w__Rp * p__a(:,i);
    L(i) = norm(a(:,i) - b(:,i));
end

base_pos = b;
platform_pos = a;

end
