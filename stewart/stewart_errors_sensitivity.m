function [max_e_L, min_e_L] = stewart_errors_sensitivity(base, platform, qe0)
min_e_L = inf; max_e_L = -inf;
q0 = qe_to_q(qe0);
L0 = stewart_invkin(base, platform, q0);
max_e_position = 0.01 * 1e-3; % 0.01 mm
max_e_orientation = ( 30 / 3600 ) * ( pi / 180 ); % 30''
max_e = zeros(6,1);
max_e(1:3) = max_e_position;
max_e(4:6) = max_e_orientation;
diapasone = [-1, 1];
for kx = diapasone
  for ky = diapasone
    for kz = diapasone
      for kphi = diapasone
        for ktheta = diapasone
          for kpsi = diapasone
            dqe = [kx; ky; kz; kphi; ktheta; kpsi] .* max_e;
            qe = qe0 + dqe;
            q = qe_to_q(qe);
            L = stewart_invkin(base, platform, q);
            [max_e_L, min_e_L] = update_e_Ls(L0, L, max_e_L, min_e_L);
          end
        end
      end
    end
  end
end

end

function [max_e_L, min_e_L] = update_e_Ls(L0, L, max_e_L_curr, min_e_L_curr)
e_L = L - L0;
max_e_L = max( [ max(e_L) , max_e_L_curr ] );
min_e_L = min( [ min(e_L) , min_e_L_curr ] );
end
