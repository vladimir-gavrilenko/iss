function [ q, n ] = stewart_forkin( base, platform, L0 )
q = [0; 0; 0.4; eul_to_quat([0, 0, 0])']; % initial etimation
F = stewart_invkin(base, platform, q) - L0;
eps = 1e-6; n = 0;
while ( norm(F) > eps )
    Fq = inv_jacobian_Fq(base, platform, q);
    q = q - (pinv(Fq) * F);
    L = stewart_invkin(base, platform, q);
    F = L - L0;
    n = n + 1;
end

end

function [ Fq ] = inv_jacobian_Fq( base, platform, q )
Fq = zeros(6,7);
U = zeros(6,3);
as = zeros(3,3,6);
[L, b0, p0] = stewart_invkin(base, platform, q);

for i =1:1:6
    U(i,:) = ((p0(:,i) - b0(:,i))./L(i))';
    as(:,:,i) = skew(b0(:,i));
end

e0 = q(4); e1 = q(5); e2 = q(6); e3 = q(7);
R = quat_to_rotm(q(4:7)');
G = [ -e1    e0    e3   -e2
      -e2   -e3    e0    e1
      -e3    e2   -e1    e0 ];

for i = 1:1:6
    Fq(i,:) = [U(i,:), -2 * U(i,:) * R * as(:,:,i) * G];
end

end

function [ as ] = skew( a )
as = [0 -a(3) a(2) ; a(3) 0 -a(1) ; -a(2) a(1) 0 ];
end
