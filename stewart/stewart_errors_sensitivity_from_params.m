function [ S ] = stewart_errors_sensitivity_from_params(Rb, Rp, Cb, Cp, H0, dx0, dy0, dz0, da0)
[base, platform] = stewart_hexagons(Rb, Rp, Cb, Cp);
qe0 = [0; 0; H0; 0; 0; 0];

% x_diapasone = [-1, 0, 1] .* dx0;
% y_diapasone = [-1, 0, 1] .* dy0;
% z_diapasone = [-1, 0, 1] .* dz0;
% a_diapasone = [-1, 0, 1] .* da0;
x_diapasone = [-1, 1] .* dx0;
y_diapasone = [-1, 1] .* dy0;
z_diapasone = [-1, 1] .* dz0;
a_diapasone = [-1, 1] .* da0;

% qe = zeros(6, 3^6);
% max_e_L = zeros(1, 3^6);
% min_e_L = zeros(1, 3^6);
qe = zeros(6, 2^6);
max_e_L = zeros(1, 2^6);
min_e_L = zeros(1, 2^6);

i = 1;
for dx = x_diapasone
  for dy = y_diapasone
    for dz = z_diapasone
      for dphi = a_diapasone
        for dtheta = a_diapasone
          for dpsi = a_diapasone
            dqe = [dx; dy; dz; dphi; dtheta; dpsi];
            qe(:,i) = qe0 + dqe;
            i = i + 1;
          end
        end
      end
    end
  end
end

N = max(size(qe));
for i = 1 : 1 : N
    [max_e_L(i), min_e_L(i)] = stewart_errors_sensitivity(base, platform, qe(:,i));
end

S = 1e-6 ./ min(abs(min_e_L));

end
