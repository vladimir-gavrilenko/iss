function [ base, platform ] = stewart_hexagons( rb, rp, Cb, Cp )
coder.inline('never');
base = zeros(6,2);
platform = zeros(6,2);

phi = 0;
dphi_b = asin( Cb / rb / 2 );
dphi_p = asin( Cp / rp / 2 );
for i = 1 : 2 : 6
    base(i,:)   = [ rb*cos(phi-dphi_b),  rb*sin(phi-dphi_b)];
    base(i+1,:) = [ rb*cos(phi+dphi_b),  rb*sin(phi+dphi_b)];

    platform(i,:)   = [ rp*cos(phi-pi/3+dphi_p), rp*sin(phi-pi/3+dphi_p)];
    platform(i+1,:) = [ rp*cos(phi+pi/3-dphi_p), rp*sin(phi+pi/3-dphi_p)];

    phi = phi + 2*pi/3;
end

end
