function [ax, ay] = stewart_2DB_joint_angle(base_pos, platform_pos, leg_index)
%[ax, ay] = STEWART_2DB_JOINT_ANGLE(base_pos, platform_pos, leg_index)
%
%������� ���������:
%   * base_pos - ������� 3�6 ��������� ����� ��������� ��� � ���� � ������� ��
%
%   * platform_pos -
%                ������� 3�6 ��������� ����� ��������� ��� � ���������
%                � ������� ��
%
%   * leg_index -
%                ����� ����, ���� �������� ������� �����������
%
%�������� ��������:
%   * ax      - ���� ������� ������ ��� ��������� ��� x
%
%   * ay      - ���� ������� ������ ��� ��������� ��� y
%
i = leg_index;
rb = norm(base_pos(:,i)); % ������ ���������� ����

% ���������� ����� ��������� ��������������� ���� � ���� (� ������� ��)
b__pb = base_pos(:,i);

% ���������� ����� ��������� ��������������� ���� � ��������� (� ������� ��)
b__pp = platform_pos(:,i);

% ����� i-� ����
Li = norm(b__pp - b__pb);

% ���� ����� ������� �� � ��, ��������� � ������ ��������� i-� ���� � ����
phi = atan2(b__pb(2), b__pb(1));

T_rot_z =   [ cos(phi)   -sin(phi)   0          0
              sin(phi)    cos(phi)   0          0
              0           0          1          0
              0           0          0          1 ];
T_shift_x = [ 1           0          0          rb
              0           1          0          0
              0           0          1          0
              0           0          0          1 ];
% ������� �������� �� ������� �� � ��, ��������� � ������ ��������� i-� ���� � ����
b__T_i =  T_rot_z * T_shift_x;

% ���������� ����� ��������� i-� ���� � ��������� � ��, ��������� � ������
% ��������� i-� ���� � ����
i__pp = b__T_i \ [b__pp; 1];

% ��������� ����� ��������� ��������������� ���� � ��������� � ��, ���������
% � ��������, ������������ ������ ������� ����, �.�.
% uvw__pp = [0; 0; Li]; �����, ����� ��������
% i__pp = uvw__T_i * uvw__pp; (1)
% ��� uvw__T_i - ������� �������������� �������
% �������� 2 ��������:
% 1. uvw__T_i = T_rot_x * T_rot_y;
% 2. uvw__T_i = T_rot_y * T_rot_x;
% �� (1) ��������� ������� ����

% 1 �������: uvw__T_i = T_rot_x * T_rot_y =
% [          cos(ay),       0,          sin(ay), 0]
% [  sin(ax)*sin(ay), cos(ax), -cos(ay)*sin(ax), 0]
% [ -cos(ax)*sin(ay), sin(ax),  cos(ax)*cos(ay), 0]
% [                0,       0,                0, 1]
%  sin(ay) * Li = i__pp(1);
% -cos(ay) * sin(ax) * Li = i__pp(2);
%  cos(ax) * cos(ay) * Li = i__pp(3);
ay = asin( i__pp(1) / Li );
ax = asin( i__pp(2) / ( -cos(ay) * Li ) );

% 2 �������: uvw__T_i = T_rot_y * T_rot_x =
% [  cos(ay), sin(ax)*sin(ay), cos(ax)*sin(ay), 0]
% [        0,         cos(ax),        -sin(ax), 0]
% [ -sin(ay), cos(ay)*sin(ax), cos(ax)*cos(ay), 0]
% [        0,               0,               0, 1]
%  cos(ax) * sin(ay) * Li = i__pp(1);
% -sin(ax) * Li = i__pp(2);
%  cos(ax) * cos(ay) * Li = i__pp(3);
% ax = asin( i__pp(2) / Li );
% ay = asin( i__pp(1) / ( cos(ax) * Li ) );

end
