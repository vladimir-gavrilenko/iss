% clear, clc
rp = 0.150; % radius of the platform
rb = 0.175; % radius of the base
h  = 0.300; % height in initial position
Cb = 0.100; % base attachment points distance
Cp = 0.100; % platform attachment points distance
[base, platform] = stewart_hexagons(rb, rp, Cb, Cp);

q = zeros(7,10); q_out = zeros(7,10);
q0 = [0; 0; h; eul_to_quat([0, 0, 0])']; % 0
qe0 = [0; 0; h; 0; 0; 0];

KR = 0.1; DR = KR.*h; DA = pi/10;
for i = 1:1:10
    dr = -2.*DR.*rand(3,1) + DR;
    da = -2.*DA.*rand(3,1) + DA;
    dqe = [dr; da];
    qe = qe0 + dqe;
    q(:,i) = qe_to_q(qe);
end

n = zeros(10,1);
L = zeros(6,10);
for i = 1:1:10
    L(:,i) = stewart_invkin(base, platform, q(:,i));
    [q_out(:,i), n(i)] = stewart_forkin(base, platform, L(:,i));
    [tmp, b, p] = stewart_invkin(base, platform, q_out(:,i));
    h = stewart_draw(b, p);
%     file_name = strcat('forkin_fig_', num2str(i));
%     saveas(h, file_name, 'png');
    pause(0.5)
end
