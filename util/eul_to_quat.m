function q = eul_to_quat( eul )
coder.inline('never');
c = cos(eul/2);
s = sin(eul/2);

q = [c(:,1).*c(:,2).*c(:,3)+s(:,1).*s(:,2).*s(:,3), ...
     c(:,1).*c(:,2).*s(:,3)-s(:,1).*s(:,2).*c(:,3), ...
     c(:,1).*s(:,2).*c(:,3)+s(:,1).*c(:,2).*s(:,3), ...
     s(:,1).*c(:,2).*c(:,3)-c(:,1).*s(:,2).*s(:,3)];

end

