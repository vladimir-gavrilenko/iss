function [ eul ] = rotm_to_eul( R )
eul = calculateEulerAngles(R, 'ZYX');
eul = reshape(eul,[3, numel(eul)/3]).';
end

function eul = calculateEulerAngles(R, seq)

persistent nextAxis seqSettings
if isempty(nextAxis)
    nextAxis = [2, 3, 1, 2];
    seqSettings = containers.Map('KeyType','char','ValueType','any');    
    seqSettings('ZYX') = [1, 0, 0, 1];
    seqSettings('ZYZ') = [3, 1, 1, 1];
end

setting = seqSettings(seq);
firstAxis = setting(1);
repetition = setting(2);
parity = setting(3);
movingFrame = setting(4);

i = firstAxis;
j = nextAxis(i+parity);
k = nextAxis(i-parity+1);

if repetition
    sy = sqrt(R(i,j,:).*R(i,j,:) + R(i,k,:).*R(i,k,:));    
    singular = sy < 10 * eps(class(R));
    eul = [atan2(R(i,j,:), R(i,k,:)), atan2(sy, R(i,i,:)), atan2(R(j,i,:), -R(k,i,:))];
    if any(singular)
        eul(:,:,singular) = [atan2(-R(j,k,singular), R(j,j,singular)), ...
            atan2(sy(singular), R(i,i,singular)), 0];
    end
    
else
    sy = sqrt(R(i,i,:).*R(i,i,:) + R(j,i,:).*R(j,i,:));    
    singular = sy < 10 * eps(class(R));
    eul = [atan2(R(k,j,:), R(k,k,:)), atan2(-R(k,i,:), sy), atan2(R(j,i,:), R(i,i,:))];
    if any(singular)
        eul(:,:,singular) = [atan2(-R(j,k,singular), R(j,j,singular)), ...
            atan2(-R(k,i,singular), sy(singular)), 0];
    end    
end

if parity
    eul = -eul;
end

if movingFrame
    eul(:,[1,3],:)=eul(:,[3,1],:);
end

end