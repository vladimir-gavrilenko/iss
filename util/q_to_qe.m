function [ qe ] = q_to_qe( q )
coder.inline('never');
qe = [q(1:3); quat_to_eul(q(4:7)')'];
end

