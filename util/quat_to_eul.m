function [ eul ] = quat_to_eul( q )
coder.inline('never');
q = bsxfun(@times, q, 1./sqrt(sum(q.^2,2)));
qw = q(:,1);
qx = q(:,2);
qy = q(:,3);
qz = q(:,4);

eul = [ atan2( 2*(qx.*qy+qw.*qz), qw.^2 + qx.^2 - qy.^2 - qz.^2 ), ...
        asin( -2*(qx.*qz-qw.*qy) ), ...
        atan2( 2*(qy.*qz+qw.*qx), qw.^2 - qx.^2 - qy.^2 + qz.^2 )];
end

