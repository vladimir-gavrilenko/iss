function [ q ] = qe_to_q( qe )
coder.inline('never');
q = [qe(1:3); eul_to_quat(qe(4:6)')'];
end
