function [ q, n, phi ] = tripod_3rps_forkin( base, platform, L0 )

R = base(1,1);
r = platform(1,1);

eps = 1e-12; phi0 = [pi/2; pi/2; pi/2];
[phi, n] = nk_find_revolute_angles(phi0, L0, eps, R, r);

xi = [ R-L0(1)*cos(phi(1)),  -0.5*(R-L0(2)*cos(phi(2))),         -0.5*(R-L0(3)*cos(phi(3)));
       0,                     0.5*sqrt(3)*(R-L0(2)*cos(phi(2))), -0.5*sqrt(3)*(R-L0(3)*cos(phi(3)));
       L0(1)*sin(phi(1)),     L0(2)*sin(phi(2)),                  L0(3)*sin(phi(3));              ];
xc = ( xi(:,1) + xi(:,2) + xi(:,3) ) ./ 3;

% LL2 = L.^2 + r^2 - 2.*L(1).*r.*cos(phi);

eul = find_euler_angles(xi, xc, r);

q =[xc; eul];

end

function [ eul ] = find_euler_angles(xi, xc, r)

ux = (xi(1,1) - xc(1)) ./ r;
uy = (xi(2,1) - xc(2)) ./ r;
uz = (xi(3,1) - xc(3)) ./ r;

vx = (xi(1,2) - xc(1) + 0.5.*r.*ux) ./ (0.5.*sqrt(3).*r);
vy = (xi(2,2) - xc(2) + 0.5.*r.*uy) ./ (0.5.*sqrt(3).*r);
vz = (xi(3,2) - xc(3) + 0.5.*r.*uz) ./ (0.5.*sqrt(3).*r);

K1 = ( ux*vz - uz*vx ) / ( uy*vx - ux*vy );
K2 = ( ( uz + uy*K1 ) / ux ) ^ 2;

wz = sqrt( 1 / ( K2 + K1^2 + 1) );
wy = K1 * wz;
wx = ( -uz*wz - uy*wy ) / ux;

R = [ux, vx, wx; uy, vy, wy; uz, vz, wz];
eul = rotm_to_eul(R)';
end

function [ phi, n ] = nk_find_revolute_angles(phi0, L0, eps, R, r)
phi = phi0;
n = 0;
F = get_F( phi, L0, R, r );
while norm(F) > eps && n < 20000
    dF = get_dF( phi, L0, R, r );
    
    dF1 = [-F, dF(:,2), dF(:,3)];
    dF2 = [dF(:,1), -F, dF(:,3)];
    dF3 = [dF(:,1), dF(:,2), -F];
    
    phi(1) = det(dF1)/det(dF) + phi(1);
    phi(2) = det(dF2)/det(dF) + phi(2);
    phi(3) = det(dF3)/det(dF) + phi(3);
    
    F = get_F( phi, L0, R, r );
    n = n + 1;    
end

end

function [ dF ] = get_dF( phi, L, R, r )
dF = zeros(3,3);

dF(1,1) = 1.5*(r - L(1)*cos(phi(1)))*L(1)*sin(phi(1)) + ...
            (1.5*r - L(2)*cos(phi(2)) - 0.5*L(1)*cos(phi(1)))*L(1)*sin(phi(1)) - ...
            2*(L(2)*sin(phi(2)) - L(1)*sin(phi(1)))*L(1)*cos(phi(1));

dF(1,2) = 2*(1.5*r - L(2)*cos(phi(2)) - 0.5*L(1)*cos(phi(1)))*L(2)*sin(phi(2)) + ...
            2*(L(2)*sin(phi(2)) - L(1)*sin(phi(1)))*L(2)*cos(phi(2));

dF(1,3) = 0;

dF(2,1) = 0;

dF(2,2) = 2*(1.5*r - L(2)*cos(phi(2)) - 0.5*L(3)*cos(phi(3)))*L(2)*sin(phi(2)) - ...
            2*(L(3)*sin(phi(3)) - L(2)*sin(phi(2)))*L(2)*cos(phi(2));

dF(2,3) = 1.5*(r - L(3)*cos(phi(3)))*L(3)*sin(3) + ...
            (1.5*r - L(2)*cos(phi(2)) - 0.5*L(3)*cos(phi(3)))*L(3)*sin(3) + ...
            2*(L(3)*sin(phi(3)) - L(2)*sin(phi(2)))*L(3)*cos(phi(3));

dF(3,1) = -1.5*(L(1)*cos(phi(1)) + L(3)*cos(phi(3)) - 2*r)*L(1)*sin(phi(1)) - ...
            0.5*(L(1)*cos(phi(1)) - L(3)*cos(phi(3)))*L(1)*sin(phi(1)) + ...
            2*(L(1)*sin(phi(1)) - L(3)*sin(phi(3)))*L(1)*cos(phi(1));

dF(3,2) = 0;

dF(3,3) = -1.5*(L(1)*cos(phi(1)) + L(3)*cos(phi(3)) - 2*r)*L(3)*sin(phi(3)) - ...
            0.5*(L(1)*cos(phi(1)) - L(3)*cos(phi(3)))*L(3)*sin(phi(3)) + ...
            2*(L(1)*sin(phi(1)) - L(3)*sin(phi(3)))*L(3)*cos(phi(3));
end

function [ F ] = get_F( phi, L, R, r )
F = [0; 0; 0];
F(1) = (r*0.5*sqrt(3) - 0.5*sqrt(3)*L(1)*cos(phi(1)))^2 + ...
        (1.5*r - L(2)*cos(phi(2)) - 0.5*L(1)*cos(phi(1)))^2 + ...
        (L(2)*sin(phi(2)) - L(1)*sin(phi(1)))^2 - 3*R^2;

F(2) = (r*0.5*sqrt(3) - 0.5*sqrt(3)*L(3)*cos(phi(3)))^2 + ...
        (- 1.5*r + L(2)*cos(phi(2)) + 0.5*L(3)*cos(phi(3)))^2 + ...
        (L(3)*sin(phi(3)) - L(2)*sin(phi(2)))^2 - 3*R^2;

F(3) = (-r*sqrt(3) + 0.5*sqrt(3)*L(1)*cos(phi(1)) + 0.5*sqrt(3)*L(3)*cos(phi(3)))^2 + ...
        (0.5*L(1)*cos(phi(1)) - 0.5*L(3)*cos(phi(3)))^2 + ...
        (L(1)*sin(phi(1)) - L(3)*sin(phi(3)))^2 - 3*R^2;
end
