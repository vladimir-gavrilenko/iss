function [ base, platform ] = stewart_hexagons( rb, rp )

base =     [     rb,              0
            -0.5*rb,  0.5*sqrt(3)*rb
            -0.5*rb, -0.5*sqrt(3)*rb ];

platform = [     rp,              0
            -0.5*rp,  0.5*sqrt(3)*rp
            -0.5*rp, -0.5*sqrt(3)*rp ];

end
