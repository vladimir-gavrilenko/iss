function [L, base_pos, platform_pos] = tripod_3rps_invkin( base, platform, q )
L = zeros(3,1);
base_pos = zeros(3,3);
platform_pos = zeros(3,3);
delete_last_row = [ 1 0 0 0; 0 1 0 0; 0 0 1 0 ];
xx = q(1); yy = q(2); zz = q(3);
R = eul_to_rotm(q(4:6)');

Tp = [R, [xx; yy; zz]; 0 0 0 1];
Tb = eye(4);

for i = 1 : 1 : 3
    platform_pos(:,i) = delete_last_row*(Tp * [ platform(i,:)' ; 0; 1; ]);
    base_pos(:,i)     = delete_last_row*(Tb * [ base(i,:)'     ; 0; 1; ]);

    L(i) = sqrt((platform_pos(1,i) - base_pos(1,i))^2 + ...
                (platform_pos(2,i) - base_pos(2,i))^2 + ...
                (platform_pos(3,i) - base_pos(3,i))^2 );
end

end
