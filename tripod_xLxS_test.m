clear, clc
rb = 0.175;
rp = 0.175;
H0 = 0.400;

[base, platform] = tripod_xLxS_triangles( rb, rp );

%% 1. 2L1S
L0 = [H0; H0; H0];
dLs = [-1, 0, 1] .* L0(1) .* 0.2;
N = length(dLs)^2;
L = zeros(3,N);
i = 1;
q = zeros(7,N); qe = zeros(6,N);
count = zeros(1,N);
for dL2 = dLs
    for dL3 = dLs
        L(:,i) = L0 + [0; dL2; dL3];
        [q(:,i), count(i)] = tripod_xLxS_forkin(base, platform, L(:,i));
        qe(:,i) = q_to_qe(q(:,i));
        i = i + 1;
    end
end

quat = q(4:7,:);
R = zeros(3,3,N);
a = zeros(1,N); e = zeros(1,N); n = zeros(1,N);
for i = 1 : 1 : N
    R(:,:,i) = quat_to_rotm(quat(:,i)');
    e(i) = asin(R(1,3,i));
%     a(i) = asin(-R(1,2,i)/cos(e(i)));
    n(i) = acos(R(3,3,i)/cos(e(i)));
end
test_num = 1:1:N;
T_2L1S = table(test_num', L(1,:)', L(2,:)', L(3,:)', e', n', count');
writetable(T_2L1S, 'T_2L1S.xlsx');

%% 2. 3L1S
dalpha = [-1, 0, 1] .* 15 .* pi ./ 180;
N = length(dalpha)^2;
L = zeros(3,N);
i = 1;
q = zeros(7,N); qe = zeros(6,N);
qout = zeros(7,N); Lout = zeros(3,N);
count = zeros(1,N);
% R = zeros(3,3,N);
for dn = dalpha
    for de = dalpha
        n(i) = dn; e(i) = de; a(i) = 0;
        R3 = [cos(a(i)) -sin(a(i)) 0
              sin(a(i))  cos(a(i)) 0
              0       0      1];
        R2 = [cos(e(i)) 0 sin(e(i)) 
              0      1   0
            -sin(e(i))  0 cos(e(i))];
        R1 = [1   0     0
              0 cos(n(i)) -sin(n(i))
              0 sin(n(i))  cos(n(i))];
        R = R1*R2*R3;
        eul = rotm_to_eul(R)';
        q(:,i) = qe_to_q([0; 0; H0; eul]);
        L(:,i) = tripod_xLxS_invkin(base, platform, q(:,i));
        [qout(:,i), count(i)] = tripod_xLxS_forkin(base, platform, L(:,i));
        [Lout(:,i), bp, pp] = tripod_xLxS_invkin(base, platform, qout(:,i));
%         tripod_3L1S_draw(bp, pp);
%         pause(0.2);
        i = i + 1;
    end
end

T_3L1S = table(test_num', L(1,:)', L(2,:)', L(3,:)', e', n', count');
writetable(T_3L1S, 'T_3L1S.xlsx');

%% 3. 3L0S

dalpha = [-1, 0, 1] .* 15 .* pi ./ 180;
dzs = [-1, 0, 1] .* H0 .* 0.2;
N = length(dalpha)^3;
L = zeros(3,N);
i = 1;
q = zeros(7,N); qe = zeros(6,N);
qout = zeros(7,N); Lout = zeros(3,N);
count = zeros(1,N); z = zeros(1,N);
% R = zeros(3,3,N);
for dz = dzs
for dn = dalpha
    for de = dalpha        
        n(i) = dn; e(i) = de; a(i) = 0;
        z(i) = H0 + dz;
        R3 = [cos(a(i)) -sin(a(i)) 0
              sin(a(i))  cos(a(i)) 0
              0       0      1];
        R2 = [cos(e(i)) 0 sin(e(i)) 
              0      1   0
            -sin(e(i))  0 cos(e(i))];
        R1 = [1   0     0
              0 cos(n(i)) -sin(n(i))
              0 sin(n(i))  cos(n(i))];
        R = R1*R2*R3;
        eul = rotm_to_eul(R)';
        q(:,i) = qe_to_q([0; 0; z(i); eul]);
        L(:,i) = tripod_xLxS_invkin(base, platform, q(:,i));
        [qout(:,i), count(i)] = tripod_xLxS_forkin(base, platform, L(:,i));
        [Lout(:,i), bp, pp] = tripod_xLxS_invkin(base, platform, qout(:,i));
%         tripod_3L1S_draw(bp, pp);
%         pause(0.2);
        i = i + 1;
    end
end
end
test_num = 1:1:N;
T_3L0S = table(test_num', L(1,:)', L(2,:)', L(3,:)', z', e', n', count');
writetable(T_3L0S, 'T_3L0S.xlsx');
