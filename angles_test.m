clear, clc
%% 1. � ���� ������������ ax_i == 0, ay_i == -45 deg.
rp = 0.175 / 2;
rb = 0.175;
h  = 0.175 / 2;
Cb = rb;
Cp = rp;
[base, platform] = stewart_hexagons(rb, rp, Cb, Cp);
q0 = [0; 0; h; eul_to_quat([0, 0, 0])'];
[L0, bp, pp] = stewart_invkin(base, platform, q0);
stewart_draw(bp, pp);

ax = zeros(1,6);
ay = zeros(1,6);
for i = 1 : 1 : 6
    [ax(i), ay(i)] = stewart_2DB_joint_angle(bp, pp, i);
end
disp([ax; ay] .* 180 ./ pi);

%% 2. � ���� ������������ ax_3 == 45, ay_3 == 0
rp = 0.175;
rb = 0.175;
h  = 0.5;
Cb = rb;
Cp = rp;
[base, platform] = stewart_hexagons(rb, rp, Cb, Cp);
q0 = [h; 0; h; eul_to_quat([0, 0, 0])'];
[L0, bp, pp] = stewart_invkin(base, platform, q0);
stewart_draw(bp, pp);

ax = zeros(1,6);
ay = zeros(1,6);
for i = 1 : 1 : 6
    [ax(i), ay(i)] = stewart_2DB_joint_angle(bp, pp, i);
end
disp([ax; ay] .* 180 ./ pi);
